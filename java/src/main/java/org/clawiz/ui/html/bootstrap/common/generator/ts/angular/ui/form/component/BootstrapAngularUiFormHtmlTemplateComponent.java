/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */



package org.clawiz.ui.html.bootstrap.common.generator.ts.angular.ui.form.component;


import org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.component.field.Field;
import org.clawiz.ui.common.metadata.data.layout.container.column.Column;
import org.clawiz.ui.common.metadata.data.layout.container.row.Row;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form.component.AngularUiFormHtmlTemplateComponent;
import org.clawiz.ui.html.bootstrap.common.generator.ts.angular.ui.common.component.element.BootstrapAngularHtmlButtonElement;
import org.clawiz.ui.html.bootstrap.common.generator.ts.angular.ui.form.component.element.*;

public class BootstrapAngularUiFormHtmlTemplateComponent extends AngularUiFormHtmlTemplateComponent {

    @Override
    protected void prepareLayoutComponentsMap() {
        super.prepareLayoutComponentsMap();
        addLayoutComponentMap(FormLayoutComponent.class, BootstrapAngularHtmlFormContainerElement.class);
        addLayoutComponentMap(Field.class, BootstrapAngularHtmlFormFieldElement.class);
        addLayoutComponentMap(GridLayoutComponent.class, BootstrapAngularHtmlFormGridComponentElement.class);
        addLayoutComponentMap(Row.class, BootstrapAngularHtmlFormRowElement.class);
        addLayoutComponentMap(Column.class, BootstrapAngularHtmlFormColumnElement.class);
        addLayoutComponentMap(Button.class,  BootstrapAngularHtmlButtonElement.class);
    }

}
