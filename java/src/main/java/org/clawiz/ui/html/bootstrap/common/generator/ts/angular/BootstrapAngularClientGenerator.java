/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.bootstrap.common.generator.ts.angular;

import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.ui.form.TypeScriptUiFormGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.grid.TypeScriptUiGridGenerator;
import org.clawiz.ui.html.angular.common.generator.ts.angular.AngularClientGenerator;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.AngularApplicationComponentsModuleComponent;
import org.clawiz.ui.html.bootstrap.common.generator.ts.angular.application.component.BootstrapAngularApplicationComponentsModuleComponent;
import org.clawiz.ui.html.bootstrap.common.generator.ts.angular.ui.form.BoostrapAngularClientFormGenerator;
import org.clawiz.ui.html.bootstrap.common.generator.ts.angular.ui.grid.BoostrapAngularClientGridGenerator;

public class BootstrapAngularClientGenerator extends AngularClientGenerator {

    @Override
    public <T extends AbstractApplicationGeneratorContext> Class<T> getApplicationGenerateContextClass() {
        return (Class<T>) BootstrapAngularClientGeneratorContext.class;
    }

    @Override
    public <T extends TypeScriptUiFormGenerator> Class<T> getFormGeneratorClass() {
        return (Class<T>) BoostrapAngularClientFormGenerator.class;
    }

    @Override
    public <T extends TypeScriptUiGridGenerator> Class<T> getGridGeneratorClass() {
        return (Class<T>) BoostrapAngularClientGridGenerator.class;
    }

    @Override
    protected void prepareComponentsMap() {
        super.prepareComponentsMap();
        addComponentMap(AngularApplicationComponentsModuleComponent.class, BootstrapAngularApplicationComponentsModuleComponent.class);
    }
}
