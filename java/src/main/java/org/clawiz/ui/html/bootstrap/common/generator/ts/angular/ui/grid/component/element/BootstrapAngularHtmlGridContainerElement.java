/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.bootstrap.common.generator.ts.angular.ui.grid.component.element;

import org.clawiz.core.common.CoreException;
import org.clawiz.ui.common.metadata.data.layout.align.HorizontalAlign;
import org.clawiz.ui.common.metadata.data.layout.align.VerticalAlign;
import org.clawiz.ui.common.metadata.data.layout.component.toolbar.Toolbar;
import org.clawiz.ui.html.angular.common.generator.ts.angular.ui.grid.component.element.AngularHtmlGridContainerElement;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.table.*;

public class BootstrapAngularHtmlGridContainerElement extends AngularHtmlGridContainerElement {

    @Override
    protected String getHorizontalAlignToClass(HorizontalAlign align) {
        switch ( align ) {
            case LEFT   : return "text-left";
            case CENTER : return "text-center";
            case RIGHT  : return "text-right";
            default: throw new CoreException("Horizontal align '?' not supported for bootstrap tables", align.toString());
        }
    }

    @Override
    protected String getVerticalAlignToClass(VerticalAlign align) {
        switch ( align ) {
            case TOP    : return null;
            default: throw new CoreException("Vertical align '?' not supported for bootstrap tables", align.toString());
        }
    }

    @Override
    protected HtmlTableElement addTable(AbstractHtmlBodyElement content) {
        HtmlTableElement table =  super.addTable(content);

        table.addClass("table table-striped table-bordered table-hover clawiz-paged-table");

        AbstractHtmlBodyElement div        = content.div().withClass("pull-right");
        AbstractHtmlBodyElement pagination = div.addElement(AbstractHtmlBodyElement.class);
        pagination.setTagName("pagination");
        pagination.setSelfCloseEnabled(false);
        pagination.setClass("clawiz-pagination-ul");
        pagination.setAttribute("[totalItems]",  "totalRowsCount");
        pagination.setAttribute("[itemsPerPage]", "rowsPerPage");
        pagination.setAttribute("[(ngModel)]",   "currentPage");

        pagination.setAttribute("[firstText]",    "firstPageText");
        pagination.setAttribute("[lastText]",     "lastPageText");
        pagination.setAttribute("[previousText]", "previousPageText");
        pagination.setAttribute("[nextText]",     "nextPageText");

        pagination.setAttribute("[rotate]",         "false");
        pagination.setAttribute("[maxSize]",        "5");
        pagination.setAttribute("[boundaryLinks]",  "true");
        pagination.setAttribute("[directionLinks]", "true");

        pagination.setAttribute("[disabled]", "! controlsEnabled");

        return table;
    }

    @Override
    protected AbstractHtmlBodyElement addToolbarPartContainer(AbstractHtmlBodyElement container, HorizontalAlign align, Toolbar toolbar) {
        AbstractHtmlBodyElement toolbarPartContainer = super.addToolbarPartContainer(container, align, toolbar);
        toolbarPartContainer.setSelfCloseEnabled(false);

        if ( align == HorizontalAlign.LEFT ) {
            toolbarPartContainer.addClass("pull-left");
        } else if ( align == HorizontalAlign.RIGHT ) {
            toolbarPartContainer.addClass("pull-right");
        } else {
            throwException("Wrong toolbar part horizontal align ? in grid ?", align.toString(), getGrid().getFullName());
        }

        return toolbarPartContainer;
    }

    @Override
    protected AbstractHtmlBodyElement addTopToolbar(AbstractHtmlBodyElement content) {
        AbstractHtmlBodyElement toolbarContainer = super.addTopToolbar(content);
        toolbarContainer.setSelfCloseEnabled(false);
        return toolbarContainer;
    }

    @Override
    protected AbstractHtmlBodyElement addBottomToolbar(AbstractHtmlBodyElement content) {
        AbstractHtmlBodyElement div = super.addBottomToolbar(content);
        div.setSelfCloseEnabled(false);
        return div;
    }
}
