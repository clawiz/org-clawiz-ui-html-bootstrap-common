/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.bootstrap.common.generator.servlet.view.form.servlet.component;


import org.clawiz.ui.common.metadata.data.layout.component.FormLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.button.Button;
import org.clawiz.ui.common.metadata.data.layout.container.column.Column;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.html.bootstrap.common.generator.servlet.view.common.component.element.html.JavaServletBootstrapButtonElement;
import org.clawiz.ui.html.bootstrap.common.generator.servlet.view.common.component.element.html.JavaServletBootstrapFormElement;
import org.clawiz.ui.html.bootstrap.common.generator.servlet.view.form.servlet.component.element.BootstrapFormColumnElement;
import org.clawiz.ui.html.common.generator.servlet.view.form.servlet.component.FormHtmlServletPrototypeComponent;

public class BootstrapFormServletPrototypeComponent extends FormHtmlServletPrototypeComponent {

    @Override
    protected void prepareLayoutComponentToElementMap() {
        super.prepareLayoutComponentToElementMap();
        
        setLayoutComponentToElementMap(FormLayoutComponent.class, JavaServletBootstrapFormElement.class);
        
        setLayoutComponentToElementMap(Column.class, BootstrapFormColumnElement.class);

/*
        setLayoutComponentToElementMap(TextInput.class,     JavaServletBootstrapTextInputElement.class);
        setLayoutComponentToElementMap(DateInput.class,     JavaServletBootstrapDateInputElement.class);
        setLayoutComponentToElementMap(DateTimeInput.class, JavaServletBootstrapDateTimeInputElement.class);
        setLayoutComponentToElementMap(NumberInput.class,   JavaServletBootstrapNumberInputElement.class);
        setLayoutComponentToElementMap(ComboBoxInput.class,   JavaServletBootstrapComboBoxInputElement.class);
*/

        setLayoutComponentToElementMap(Button.class, JavaServletBootstrapButtonElement.class);
    }
    
}
