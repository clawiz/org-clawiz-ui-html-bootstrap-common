/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.bootstrap.common.generator.ts.angular.application.component;

import org.clawiz.ui.html.angular.common.generator.ts.angular.AngularClientGeneratorContext;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.AngularApplicationComponentsModuleComponent;

public class BootstrapAngularApplicationComponentsModuleComponent extends AngularApplicationComponentsModuleComponent {

    @Override
    public void process() {
        super.process();

        addImport("PaginationModule", "ngx-bootstrap");
        addImport("LoadingModule", "ngx-loading");
        addImport("ANIMATION_TYPES", "ngx-loading");
        addImport("ClawizMouseWheelDirective", ((AngularClientGeneratorContext) getApplicationContext()).getAngularCoreImportPath());

        getNgModule().addDeclaration("ClawizMouseWheelDirective");

        getNgModule().addImport("PaginationModule.forRoot()");
        getNgModule().addImport("LoadingModule.forRoot({animationType: ANIMATION_TYPES.threeBounce,backdropBackgroundColour: 'rgba(255,255,255,0.2)',backdropBorderRadius: '4px',primaryColour: '#c0c0c0',secondaryColour: '#d0d0d0',tertiaryColour: '#e0e0e0'})");
    }
}
